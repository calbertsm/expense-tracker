import React from "react";
import { SiderTheme } from "antd/lib/layout/Sider";

interface Themes {
	light: SiderTheme,
	dark: SiderTheme
}

export const themes : Themes = {
	light: 'light',
	dark: 'dark'
};

const ThemeContext = React.createContext({
	theme: themes.light,
	toggleTheme: () => {}
});

export const ThemeProvider = ThemeContext.Provider;
export const ThemeConsumer = ThemeContext.Consumer;

export default ThemeContext;