import { forEach, isEmpty } from "lodash";
import config from "config";
const axios = require('axios').default;

let api:any;

interface Param {
	key: string;
	value: string | boolean | number;
}

interface BatchObject {
	id: string | number;
	body: object;
}

type BatchMethod = "PUT" | "PATCH" | "POST" | "DELETE";

export function createServerInstance() {
	api = axios.create({
		baseURL: `${config.server.protocol}://${config.server.name}${isEmpty(config.server.port) ? '' : `:${config.server.port}`}`
	});
}

export default api;

export async function get(entity: string, params: object = {}): Promise<any> {
	entity = entity[0] === "/" ? entity.substring(1) : entity;
	return api.get(`/${entity}`, {
		params: params
	});
}

export async function post(entity: string, body: object): Promise<any> {
	entity = entity[0] === '/' ? entity.substring(1) : entity;
	return api.post(`/${entity}`, body);
}

export async function put(entity: string, id: string, body: object): Promise<any> {
	entity = entity[0] === '/' ? entity.substring(1) : entity;
	return api.put(`/${entity}/${id}`, body);
}

export async function patch(entity: string, id: string, body: object): Promise<any> {
	entity = entity[0] === '/' ? entity.substring(1) : entity;
	return api.patch(`/${entity}/${id}`, body);
}

export async function del(entity: string, id: string): Promise<any> {
	entity = entity[0] === '/' ? entity.substring(1) : entity;
	return api.delete(`/${entity}/${id}`);
}

export async function batch(method:BatchMethod, entity:string, params:BatchObject[]) : Promise<any>{
	let promises:Promise<any>[] = [];
	forEach(params, (param) => {
		if (method !== "POST" && isEmpty(param.id)) {
			promises.push(new Promise((resolve, reject) => {
				reject(new Error(`Cannot perform action ${method}: Missing param ID`))
			}));
		} else if (method !== "DELETE" && isEmpty(param.body)) {
			promises.push(new Promise((resolve, reject) => {
				reject(new Error(`Cannot perform action ${method}: Missing request body`))
			}));
		} else {
			promises.push(api({
				method: method,
				url: `/${entity}`,
				data: method !== "POST" && method !== "DELETE" ? param.body : {},
			}).catch((error:any) => { return error }));
		}
	});
	return Promise.all(promises);
}
