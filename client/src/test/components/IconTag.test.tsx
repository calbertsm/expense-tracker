import React from "react";
import IconTag from "components/IconTag/IconTag";
import ReactDOM from 'react-dom';

it('renders without crashing (no color)', () => {
  const div = document.createElement('div');
  ReactDOM.render(<IconTag>Test</IconTag>, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('renders without crashing (with color)', () => {
	const div = document.createElement('div');
	ReactDOM.render(<IconTag color="#b30000">Test</IconTag>, div);
	ReactDOM.unmountComponentAtNode(div);
});