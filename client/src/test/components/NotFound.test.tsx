import React from "react";
import NotFound from "components/NotFound/NotFound";
import ReactDOM from 'react-dom';

it('renders without crashing (no color)', () => {
  const div = document.createElement('div');
  ReactDOM.render(<NotFound/>, div);
  ReactDOM.unmountComponentAtNode(div);
});