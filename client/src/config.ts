const dev = {
	server: {
		protocol: "http",
		name: "localhost",
		port: "3001"
	}
};

const prd = {
	server: {
		protocol: "https",
		name: "expense-tracker-server-casm.herokuapp.com",
		port: ""
	}
};

const config = process.env.REACT_APP_ENV === "dev" ? dev : prd;

export default {
	iconFontUrl: "//at.alicdn.com/t/font_1429429_frrqcn35wn.js",
	...config
};
