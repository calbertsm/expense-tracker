import { floor, padEnd, isEmpty, findLast } from "lodash";
import moment, { Moment } from "moment";
import { ReactNode } from "react";
import { Modal } from "antd";

type SIValue = {
	base:number;
	symbol:string;
}

const SI:SIValue[] = [{ base: 1, symbol: "" }, { base: 1e3, symbol: "k" }, { base: 1e6, symbol: "M" }, { base: 1e9, symbol: "G" }, { base: 1e12, symbol: "T" }, { base: 1e15, symbol: "P" }, { base: 1e18, symbol: "E" }];

export type MessageBoxType = "info" | "success" | "error" | "warning";

export type MessageBoxParams = {
	title?: string;
	content: string | ReactNode;
	afterClose?: () => {};
	closable?: boolean;
	onCancel?: () => {};
	onOk?: () => {};
	centered?: boolean;
};

export const HexColor: RegExp = new RegExp("^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$");
export const HexColorWithourSharp: RegExp = new RegExp("^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$");

export type Category = {
	id?: string;
	name: string;
	icon: string;
	color: string;
	total?: number;
};

export type Expense = {
	id?: string;
	description: string;
	amount: number;
	notes?: string;
	date: string;
	categories?: Category[];
};

export function numberFormatter(value: any, precision: number = 2): string {
	if (!value || value === "undefined") {
		return "";
	}
	let val: string = String(value);
	let matches = val.match(/^(-?)(\d*)(\.(\d+))?$/);
	if (matches !== null) {
		let negative: string = matches[1];
		let _number: string = matches[2] || "0";
		let _decimal: string = matches[4] || "";
		_number = _number.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		let _decimalNumber: number = floor(parseFloat(`0.${_decimal}`), precision);
		_decimal = padEnd(String(_decimalNumber).substr(2), precision, "0");
		return `${negative} ${_number}.${_decimal}`.trim();
	}
	return val;
}

export function numberMinifier(number: number): string {
	let regex = new RegExp(/\.0+$|(\.[0-9]*[1-9])0+$/);
	let siSymbol:SIValue = findLast(SI, (si) => {
		return number > si.base;
	}) || SI[0];
	let result = (number / siSymbol.base).toFixed(2);
	return result.replace(regex, "$1") + siSymbol.symbol;
}

export function normalizeHexColor(colorString: string | undefined): string {
	colorString = isEmpty(colorString) ? "" : String(colorString).trim();
	if (!isEmpty(colorString)) {
		if (!HexColor.test(colorString) && HexColorWithourSharp.test(colorString)) {
			colorString = `#${colorString}`;
		}
	}
	return colorString;
}

export function getDatesFromDateRange(startDate: Moment = moment(), endDate: Moment = moment()): string[] {
	let result: string[] = [];
	if (startDate.isAfter(endDate)) {
		throw new Error("Start Date is after End Date");
	}
	let dayDiff = endDate.diff(startDate, "days");
	for (let index = 0; index <= dayDiff; index++) {
		result.push(
			moment(startDate)
				.add(index, "days")
				.format("MMM DD")
		);
	}
	return result;
}

export function showApiErrorMessage(error: any): void {
	let errorMessage: string;
	if (isEmpty(error.response)) {
		errorMessage = `Error: ${error.message}`;
	} else {
		errorMessage = `Error ${error.response.data.code} ${error.response.data.name}: ${error.response.data.message}`;
	}
	showMessage("error", {
		title: "Error",
		content: errorMessage
	});
}

export function showMessage(type: MessageBoxType, params: MessageBoxParams): void {
	if (isEmpty(params.title)) {
		params.title = "Expense Tracker";
	}
	if (isEmpty(params.closable)) {
		params.closable = true;
	}
	if (isEmpty(params.centered)) {
		params.centered = true;
	}
	switch (type) {
		case "success":
			Modal.success(params);
			break;
		case "warning":
			Modal.warning(params);
			break;
		case "error":
			Modal.error(params);
			break;
		default:
			Modal.info(params);
			break;
	}
}
