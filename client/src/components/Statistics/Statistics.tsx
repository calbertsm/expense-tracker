import React, { Component } from "react";
import "./Statistics.scss";

import { Row, Col, Icon, DatePicker, Card, Tabs, Spin, Alert } from "antd";
import { RangePickerValue } from "antd/lib/date-picker/interface";
import Chart from "react-apexcharts";
import StastisticCard from "components/StastisticCard/StastisticCard";

import moment, { Moment } from "moment";
import { getDatesFromDateRange, Expense, Category, showApiErrorMessage } from "utils/utils";
import { get } from "api";
import { capitalize, map } from "lodash";

const { TabPane } = Tabs;
const { RangePicker } = DatePicker;

interface ComplexChartSeries {
	name: string;
	data: number[];
}

type ChartSeries = ComplexChartSeries[] | number[];

interface ChartSettings {
	loaderBusy: boolean;
	options: any;
	series: ChartSeries;
}

interface StatisticsCardInfo {
	totalExpensesAllTime: number;
	biggestExpenseAllTime: Expense;
	biggestExpenseToday: Expense;
	averageExpenseThisMonth: number;
	averageExpenseLastMonth: number;
	averageExpenseToday: number;
	averageExpenseYesterday: number;
	mostExpendedCategory: Category;
}

interface State {
	dateRange: RangePickerValue;
	dateRangeDisabled: boolean;
	cardLoader: boolean;
	statistics: StatisticsCardInfo;
	chart: {
		expensesPerDay: ChartSettings;
		expensesPerCategory: ChartSettings;
		expensesJoined: ChartSettings;
	};
}

interface Props { }

export default class Statistics extends Component<Props, State> {
	constructor(props: Props) {
		super(props);
		this.state = {
			dateRangeDisabled: true,
			dateRange: [moment().startOf("week"), moment().endOf("week")],
			cardLoader: true,
			statistics: {
				totalExpensesAllTime: 0,
				biggestExpenseAllTime: {
					id: '-',
					description: '-',
					amount: 0,
					notes: '-',
					date: '-',
					categories: []
				},
				biggestExpenseToday: {
					id: '-',
					description: '-',
					amount: 0,
					notes: '-',
					date: '-',
					categories: []
				},
				averageExpenseThisMonth: 0,
				averageExpenseLastMonth: 0,
				averageExpenseToday: 0,
				averageExpenseYesterday: 0,
				mostExpendedCategory: {
					id: '-',
					name: '-',
					icon: '-',
					color: '-',
					total: 0
				},
			},
			chart: {
				expensesPerDay: {
					loaderBusy: true,
					options: {
						chart: {
							id: "expensesPerDayChart",
							toolbar:{
								show: true,
								tools: {
									download: true,
									zoom: true,
									zoomin: true,
									zoomout: true
								}
							}
						},
						xaxis: {
							categories: getDatesFromDateRange(moment().startOf("week"), moment().endOf("week"))
						},
						title: {
							text: "Expenses Per Day",
							style: {
								color: "rgba(0, 0, 0, 0.85)",
								fontSize: "16px"
							}
						}
					},
					series: [
						{
							name: "",
							data: []
						}
					]
				},
				expensesPerCategory: {
					loaderBusy: true,
					options: {
						chart: {
							id: "expensesPerCategoryChart",
							toolbar:{
								show: true,
								tools: {
									download: true,
									zoom: true,
									zoomin: true,
									zoomout: true
								}
							}
						},
						title: {
							text: "Expenses Per Category",
							style: {
								color: "rgba(0, 0, 0, 0.85)",
								fontSize: "16px"
							}
						},
						plotOptions: {
							pie: {
								expandOnClick: false,
								donut: {
									labels: {
										show: true,
										name: {
											show: false
										},
										value: {
											show: false
										}
									}
								}
							},
						},
						dataLabels: {
							enabled: true,
							dropShadow: {
								enabled: true
							}
						}
					},
					series: []
				},
				expensesJoined: {
					loaderBusy: true,
					options: {
						chart: {
							id: "expensesByDayPerCategoryChart",
							stacked: true,
							toolbar:{
								show: true,
								tools: {
									download: true,
									zoom: true,
									zoomin: true,
									zoomout: true
								}
							}
						},
						title: {
							text: "Expenses per Category by Day",
							style: {
								color: "rgba(0, 0, 0, 0.85)",
								fontSize: "16px"
							}
						},
						stroke: {
							width: 1,
							colors: ["#fff"]
						},
						xaxis: {
							categories: getDatesFromDateRange(moment().startOf("week"), moment().endOf("week"))
						},
						yaxis: {
							title: {
								text: "Total"
							},
							labels: {
								show: true
							}
						},
						dataLabels: {
							enabledOnSeries: true
						},
						tooltip: {
							enabled: true
						},
						fill: {
							opacity: 1
						},
						legend: {
							position: "top",
							horizontalAlign: "left",
							offsetX: 40
						}
					},
					series: []
				}
			}
		};
	}
	componentDidMount = () => {
		this.updateStatisticsData();
	};
	updateStatisticsData = () => {
		this.setState({
			cardLoader: true
		});
		this.updateExpenseChartData(this.state.dateRange[0], this.state.dateRange[1], this.state.dateRangeDisabled);
		this.updateCategoryData();
		get("statistics", {
			timezone: moment().format("ZZ")
		}).then((statisticsResponse) => {
			this.setState({
				cardLoader: false,
				statistics: statisticsResponse.data
			});
		}).catch((error) => {
			this.setState({
				cardLoader: false
			});
			showApiErrorMessage(error);
		});

	}
	updateCategoryData = () => {
		get('statistics/expenses/category').then((categoryResponse) => {
			this.setState({
				chart: {
					...this.state.chart,
					expensesPerCategory: {
						...this.state.chart.expensesPerCategory,
						options: {
							...this.state.chart.expensesPerCategory.options,
							labels: map(categoryResponse.data, (category) => {
								return capitalize(category.name)
							}),
							plotOptions: {
								...this.state.chart.expensesPerCategory.options.plotOptions,
								style: {
									colors: map(categoryResponse.data, (category) => {
										return `#${category.total_expenses}`
									})
								}
							}
						},
						series: map(categoryResponse.data, (category) => {
							return category.total_expenses
						})
					}
				}
			});
		}).catch((error) => {
			this.setState({
				chart: {
					...this.state.chart,
					expensesPerCategory: {
						...this.state.chart.expensesPerCategory,
						options: {
							...this.state.chart.expensesPerCategory.options,
							labels: [],
							plotOptions: {
								...this.state.chart.expensesPerCategory.options.plotOptions,
								style: {
									colors: []
								}
							}
						},
						series: []
					}
				}
			});
			showApiErrorMessage(error);
		}).finally(() => {
			this.setState({
				chart: {
					...this.state.chart,
					expensesPerCategory: {
						...this.state.chart.expensesPerCategory,
						loaderBusy: false
					}
				}
			});
		});
	}
	updateExpenseChartData = (startDate: Moment = moment(), endDate: Moment = moment(), isCustom: boolean = true) => {
		this.setState({
			dateRangeDisabled: isCustom,
			dateRange: [startDate, endDate],
			chart: {
				...this.state.chart,
				expensesPerDay: {
					...this.state.chart.expensesPerDay,
					loaderBusy: true
				},
				expensesJoined: {
					...this.state.chart.expensesJoined,
					loaderBusy: true
				}
			}
		});
		get('statistics/expenses/totals', {
			startDate: startDate.format('YYYY-MM-DD'),
			endDate: endDate.format('YYYY-MM-DD'),
			timezone: moment().format('ZZ'),
		}).then((totalsResponse) => {
			this.setState({
				chart: {
					...this.state.chart,
					expensesPerDay: {
						...this.state.chart.expensesPerDay,
						series: [{
							name: "Expenses",
							data: map(totalsResponse.data.totalsByDate, (expense) => {
								return expense.total
							})
						}]
					},
					expensesJoined: {
						...this.state.chart.expensesJoined,
						series: map(totalsResponse.data.totalsPerCategoryByDate, (category) => {
							return {
								name: capitalize(category.name),
								data: category.totalsPerDate
							}
						})
					}
				}
			});
		}).catch((error) => {
			this.setState({
				chart: {
					...this.state.chart,
					expensesPerDay: {
						...this.state.chart.expensesPerDay,
						series: []
					},
					expensesJoined: {
						...this.state.chart.expensesJoined,
						series: []
					}
				}
			});
			showApiErrorMessage(error);
		}).finally(() => {
			this.setState({
				dateRangeDisabled: isCustom,
				dateRange: [startDate, endDate],
				chart: {
					...this.state.chart,
					expensesPerDay: {
						...this.state.chart.expensesPerDay,
						loaderBusy: false,
						options: {
							...this.state.chart.expensesPerDay.options,
							xaxis: {
								categories: getDatesFromDateRange(startDate, endDate)
							}
						}
					},
					expensesJoined: {
						...this.state.chart.expensesJoined,
						loaderBusy: false,
						options: {
							...this.state.chart.expensesJoined.options,
							xaxis: {
								categories: getDatesFromDateRange(startDate, endDate)
							}
						}
					}
				}
			});
		});
	}
	onExpensePerDateChange = (activeKey: string) => {
		let dateRange: RangePickerValue = this.state.dateRange;
		let disableCustomRangePicker: boolean = true;
		switch (activeKey) {
			case "custom":
				disableCustomRangePicker = false;
				break;
			case "thisMonth":
				dateRange = [moment().startOf("month"), moment().endOf("month")];
				break;
			case "thisWeek":
			default:
				dateRange = [moment().startOf("week"), moment().endOf("week")];
				break;
		}
		this.updateExpenseChartData(dateRange[0], dateRange[1], disableCustomRangePicker);
	};
	onExpensePerDateRangeChange = (dates: RangePickerValue) => {
		this.updateExpenseChartData(dates[0], dates[1]);
	};
	render() {
		return (
			<div className='ant-custom-statistics-dashboard'>
				<Row gutter={24} type='flex'>
					<Col xl={8}>
						<StastisticCard
							loading={this.state.cardLoader}
							title={"Total Expenses"}
							value={this.state.statistics.totalExpensesAllTime}
							precision={2}
							prefix='$' />
					</Col>
					<Col xl={8}>
						<StastisticCard
							loading={this.state.cardLoader}
							title={"Average Expense This Month"}
							value={this.state.statistics.averageExpenseThisMonth}
							precision={2}
							prefix='$'
							moreDataTitle={"Last Month"}
							moreDataValue={this.state.statistics.averageExpenseLastMonth}
							moreDataValuePrefix='$' />
					</Col>
					<Col xl={8}>
						<StastisticCard
							loading={this.state.cardLoader}
							title={"Average Expense Today"}
							value={this.state.statistics.averageExpenseToday}
							precision={2}
							prefix='$'
							moreDataTitle={"Yesterday"}
							moreDataValue={this.state.statistics.averageExpenseYesterday}
							moreDataValuePrefix='$' />
					</Col>
				</Row>
				<Row gutter={24} type='flex'>
					<Col xl={16} lg={16} md={24} sm={24} xs={24}>
						<Card id='expensesPerCategory'>
							<Spin spinning={this.state.chart.expensesPerCategory.loaderBusy}>
								<Chart
									height={"300px"}
									options={this.state.chart.expensesPerCategory.options}
									type='donut'
									series={this.state.chart.expensesPerCategory.series} />
							</Spin>
						</Card>
					</Col>
					<Col xl={8} lg={8} md={24} sm={24} xs={24}>
						<Row id='superlativeStatistics' gutter={24} type='flex'>
							<Col span={24}>
								<StastisticCard
									loading={this.state.cardLoader}
									title={"Most Expended Category"}
									value={capitalize(this.state.statistics.mostExpendedCategory.name)}
									precision={2}
									suffix={
										<Icon type={this.state.statistics.mostExpendedCategory.icon} />
									} />
							</Col>
							<Col span={24}>
								<StastisticCard
									loading={this.state.cardLoader}
									title={"Biggest Expense"}
									value={this.state.statistics.biggestExpenseAllTime.amount !== 0 ? this.state.statistics.biggestExpenseAllTime.amount : "0"}
									precision={2}
									prefix='$' />
							</Col>
						</Row>
					</Col>
				</Row>
				<Row gutter={24} type='flex'>
					<Card>
						<Col span={24}>
							<Tabs
								tabBarGutter={12}
								defaultActiveKey='thisWeek'
								onChange={this.onExpensePerDateChange}
								tabBarExtraContent={
									<RangePicker
										disabled={this.state.dateRangeDisabled}
										value={this.state.dateRange}
										onChange={this.onExpensePerDateRangeChange} />
								}>
								<TabPane tab='This Week' key='thisWeek' />
								<TabPane tab='This Month' key='thisMonth' />
								<TabPane tab='Custom' key='custom' />
							</Tabs>
						</Col>
						{!this.state.dateRangeDisabled ? 
						<Col span={24} className="ant-custom-warning-col">
							<Alert
								message="WARNING: Selecting a date range to long can cause some render issues that might slow down the application's performance."
								type="warning"
								showIcon
								closable
							/>
						</Col> : ''}
						<Col span={24}>
							<Card id='expensesCharts' bordered={false}>
								<Card.Grid hoverable={false} style={{ width: "100%" }}>
									<Spin spinning={this.state.chart.expensesPerDay.loaderBusy}>
										<Chart
											height={"300px"}
											options={this.state.chart.expensesPerDay.options}
											type='bar'
											series={this.state.chart.expensesPerDay.series} />
									</Spin>
								</Card.Grid>
								<Card.Grid hoverable={false} style={{ width: "100%" }}>
									<Spin spinning={this.state.chart.expensesJoined.loaderBusy}>
										<Chart
											height={"400px"}
											options={this.state.chart.expensesJoined.options}
											type='bar'
											series={this.state.chart.expensesJoined.series} />
									</Spin>
								</Card.Grid>
							</Card>
						</Col>
					</Card>
				</Row>
			</div>
		);
	}
}
