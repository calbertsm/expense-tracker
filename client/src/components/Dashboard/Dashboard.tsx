import React, { Component } from "react";

import "./Dashboard.scss"

import { Layout, Menu, Icon, notification, Avatar, Typography} from "antd";
import { SiderTheme } from "antd/lib/layout/Sider";
import { withRouter, Switch, BrowserRouter as Router, Route, Link, RouteComponentProps, Redirect } from "react-router-dom";
import { ThemeProvider, themes } from "ThemeContext";

import Statistics from "components/Statistics/Statistics";
import History from "components/History/History";
import NotFound from "components/NotFound/NotFound";
import ExpenseAdd from "components/Expense/Add/ExpenseAdd";

import config from "config";
import { find, result, map, isEmpty, capitalize } from "lodash";
import { get } from "api";

const { Content, Footer, Sider } = Layout;
const { Paragraph, Text } = Typography;
const AlternateIcon = Icon.createFromIconfontCN({
	scriptUrl: config.iconFontUrl
});

const routes = [
	{
		key: "addExpense",
		path: "/expense/add",
		exact: true,
		component: ExpenseAdd
	},
	{
		key: "statistics",
		path: "/statistics",
		exact: true,
		component: Statistics
	},
	{
		key: "history",
		path: "/history",
		exact: true,
		component: History
	},
	{
		key: "home",
		path: "/",
		exact: true,
		redirect: {
			to: "/expense/add"
		}
	},
	{
		key: "notFound",
		component: NotFound
	}
];

interface State {
	theme: SiderTheme;
	toggleTheme?: any,
	collapsed: boolean;
	collapsedSiderMargin: number;
	currentLocation: string;
}

class RoutedDashboard extends Component<RouteComponentProps, State> {
	componentDidMount = () => {
		if (!isEmpty(this.getLocationKeybyPath(this.props.location.pathname))) {
			get('statistics/category/top').then((categoryResponse) => {
				notification.open({
					message: 'Hey, Listen!',
					duration: 10,
					description: <Paragraph>Watch out, you have been spending a bit more <Text strong>$$</Text> on <Text strong>{capitalize(categoryResponse.data.name)}</Text> lately.</Paragraph>,
					icon: <Avatar src="/img/Navi.gif" />,
				  });
			});
		}
	}
 	declareRoutes = () => {
		let declaredRoutes = map(routes, (route: any) => {
			if (route.redirect) {
				return (
					<Route key={route.key} path={route.path} exact={route.exact ? route.exact : false}>
						<Redirect to={route.redirect.to}/>
					</Route>
				);
			}
			return <Route key={route.key} path={route.path} component={route.component} exact={route.exact ? route.exact : false} />
		});
		return declaredRoutes;
	};
	collapseSideMenu = (collapsed: boolean) => {
		this.setState({
			collapsed: collapsed,
			collapsedSiderMargin: collapsed ? 80 : 200
		});
	};
	toggleTheme = (checked: boolean) => {
		this.setState({
			theme: checked ? themes.dark : themes.light
		});
	}
	getLocationKeybyPath = (path: string): string => {
		if (path === "/") {
			return "addExpense";
		}
		return result(find(routes, (route: any) => {
			return route.path === path || `${route.path}/` === path;
		}), 'key');
	}
	state: State = {
		theme: themes.dark,
		collapsed: false,
		collapsedSiderMargin: 200,
		toggleTheme: this.toggleTheme,
		currentLocation: ""
	};
	render() {
		return (
			<div className="ant-custom-dashboard">
				<Router>
					<ThemeProvider value={{
						theme: this.state.theme,
						toggleTheme: this.state.toggleTheme
					}}>
						<Layout style={{ height: "100vh" }}>
							<Sider
								className={isEmpty(this.getLocationKeybyPath(this.props.location.pathname)) ? 'ant-custom-hidden-sider' : ''}
								collapsible
								theme={this.state.theme}
								breakpoint='lg'
								collapsed={this.state.collapsed}
								onCollapse={this.collapseSideMenu}>
								<div className='logo' />
								<Menu theme={this.state.theme} defaultSelectedKeys={this.props.location ? [this.getLocationKeybyPath(this.props.location.pathname)] : ["addExpense"]} mode='inline'>
									<Menu.Item key='addExpense'>
										<Icon type='wallet' />
										<span>Add Expense</span>
										<Link to='/expense/add' />
									</Menu.Item>
									<Menu.Item key='statistics'>
										<AlternateIcon type='icon-report' />
										<span>Statistics</span>
										<Link to='/statistics' />
									</Menu.Item>
									<Menu.Item key='history'>
										<Icon type='history' />
										<span>History</span>
										<Link to='/history' />
									</Menu.Item>
								</Menu>
							</Sider>
							<Layout style={{ marginLeft: `${this.state.collapsedSiderMargin}px` }}>
								<Content style={{ margin: "24px 16px 0" }}>
									<div style={{ padding: 24, background: "#fff", minHeight: 360, height: "100%", overflow: "scroll" }}>
										<Switch>
											{this.declareRoutes()}
										</Switch>
									</div>
								</Content>
								<Footer style={{ textAlign: "center" }}>Expense Tracker by @CAlbertSM (Carlos Alberto Salgado Montoya)</Footer>
							</Layout>
						</Layout>
					</ThemeProvider>
				</Router>
			</div>
		);
	}
}

const Dashboard = withRouter(props => <RoutedDashboard {...props} />);

export default Dashboard;