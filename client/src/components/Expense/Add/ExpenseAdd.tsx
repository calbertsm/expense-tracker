import React, { Component, ReactNode } from "react";
import { Form, Spin, Input, DatePicker, InputNumber, Button, Select, Row, Col } from "antd";
import { SelectValue } from "antd/lib/select";
import { FormComponentProps } from 'antd/es/form';

import "./ExpenseAdd.scss";

import { map, capitalize, intersectionBy, isEmpty } from "lodash";
import { showApiErrorMessage, Category, Expense, showMessage } from "utils/utils";
import { get, post } from "api";
import moment, { Moment } from "moment";

const { Option } = Select;
const { TextArea } = Input;

const buttonItemLayout = {
	wrapperCol: { offset: 5 }
}

const formItemLayout = {
	labelCol: { span: 5 },
	wrapperCol: { span: 15 },
}

const formItemLayout2 = {
	labelCol: { span: 10 },
	wrapperCol: { span: 10 }
}

const formItemLayout3 = {
	labelCol: { span: 5 },
	wrapperCol: { span: 10 }
}

interface State {
	isBusy: boolean;
	disableAddButton: boolean;
	selectedCategories: SelectValue;
	selectedCategoriesObjects: Category[];
	date: Moment | null;
	categories: Category[],
	categoriesOptions: ReactNode[]
}

interface Props extends FormComponentProps { }

class ExpenseForm extends Component<Props, State> {
	state = {
		isBusy: true,
		disableAddButton: true,
		date: moment(),
		selectedCategories: [],
		selectedCategoriesObjects: [],
		categories: [],
		categoriesOptions: []
	}

	componentDidMount = () => {
		this.props.form.validateFields();
		this.props.form.setFieldsValue({
			date: moment()
		});
		this.setState({
			isBusy: true
		});
		get("/category").then((categories) => {
			let categoryOptions = map(categories.data, (category) => {
				return (
					<Option key={category.id} value={category.id} title={category.name}>
						{capitalize(category.name)}
					</Option>);
			});
			this.setState({
				isBusy: false,
				categories: categories.data,
				categoriesOptions: categoryOptions
			});
		}).catch((error) => {
			this.setState({
				isBusy: false,
				categories: [],
				categoriesOptions: []
			});
			showApiErrorMessage(error);
		});
	}
	onDateChange = (date: Moment | null, dateString: string) => {
		this.setState({
			date: isEmpty(date) ? moment() : date
		});
	}
	disableDates = (currentDate: Moment | undefined = moment()): boolean => {
		return currentDate && currentDate > moment().endOf('day');
	}
	onCategoryChange = (value: SelectValue) => {
		this.setState({
			selectedCategories: value,
			selectedCategoriesObjects: this.getCategoriesById(value)
		});
	}
	getCategoriesById = (ids: any = []) => {
		return intersectionBy(this.state.categories, map(ids, (id: SelectValue) => {
			return {
				id: String(id)
			}
		}), 'id');
	}
	checkForErrors = (fieldsError: Record<string, string[] | undefined>) => {
		return Object.keys(fieldsError).some(field => fieldsError[field]);
	}
	addExpense = () => {
		this.setState({
			isBusy: true
		});
		let newExpense: Expense = {
			description: this.props.form.getFieldValue('description'),
			amount: this.props.form.getFieldValue('amount'),
			date: this.props.form.getFieldValue('date'),
			notes: this.props.form.getFieldValue('notes'),
			categories: this.props.form.getFieldValue('categories')
		}
		post('/expense', newExpense).then(() => {
			this.props.form.resetFields();
			showMessage('success', {
				title: 'Expense',
				content: 'Your expense was added succesfully.'
			});
		}).catch((error) => {
			showApiErrorMessage(error);
		}).finally(() => {
			this.setState({
				isBusy: false
			});
		});
	}
	render() {
		const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = this.props.form;
		const descriptionError = isFieldTouched('description') && getFieldError('description');
		const amountError = isFieldTouched('amount') && getFieldError('amount');
		const dateError = isFieldTouched('date') && getFieldError('date');
		const categoriesError = isFieldTouched('categories') && getFieldError('categories');

		return (
			<div className="ant-custom-expense-add">
				<Spin spinning={this.state.isBusy} size="large">
					<Form layout="horizontal">
						<Form.Item label="Description" required validateStatus={descriptionError ? 'error' : ''} help={descriptionError || ''} {...formItemLayout}>
							{getFieldDecorator('description', {
								rules: [{ required: true, message: 'Please input a description for your expense.', whitespace: true, max: 120 }],
							})(<Input allowClear placeholder="Small description about this expense" />)}
						</Form.Item>
						<Row gutter={24} type="flex" justify="start">
							<Col xl={12} lg={12} md={12} sm={24} xs={24}>
								<Form.Item label="Amount" required validateStatus={amountError ? 'error' : ''} help={amountError || ''} {...formItemLayout2}>
									{getFieldDecorator('amount', {
										rules: [{ type: 'number', required: true, message: 'Please input an amount for your expense.' }],
									})(<InputNumber prefix="$" min={0.01} precision={2} step={0.01} max={999999999.99} style={{ width: '140px' }} />)}
								</Form.Item>
							</Col>
							<Col xl={12} lg={12} md={12} sm={24} xs={24}>
								<Form.Item label="Date" required validateStatus={dateError ? 'error' : ''} help={dateError || ''} {...formItemLayout3}>
									{getFieldDecorator('date', {
										rules: [{ type: 'object', required: true, message: 'Please select time!' }],
										initialValue: moment()
									})(<DatePicker onChange={this.onDateChange} disabledDate={this.disableDates} style={{ minWidth: '140px' }}/>)}
								</Form.Item>
							</Col>
						</Row>
						<Form.Item label="Categories" required validateStatus={categoriesError ? 'error' : ''} help={categoriesError || ''} {...formItemLayout}>
							{getFieldDecorator('categories', {
								rules: [{ required: true, message: 'Please select at least one category for your expense.' }],
							})(<Select mode="multiple" placeholder="Select at least one category" onChange={this.onCategoryChange}>
								{this.state.categoriesOptions}
							</Select>)}
						</Form.Item>
						<Form.Item label="Notes" {...formItemLayout}>
							{getFieldDecorator('notes')(<TextArea
								placeholder="Notes about this expense"
								autosize={{ minRows: 2, maxRows: 6 }} />)}
						</Form.Item>
						<Form.Item {...buttonItemLayout} className="button-container">
							<Button type="primary" onClick={this.addExpense} shape="round" disabled={this.checkForErrors(getFieldsError())}>Add</Button>
						</Form.Item>
					</Form>
				</Spin>
			</div>
		);
	}
}

const WrappedExpenseForm = Form.create({ name: 'expenseAdd' })(ExpenseForm);

export default class ExpenseAdd extends Component {
	render() {
		return <WrappedExpenseForm />;
	}
}
