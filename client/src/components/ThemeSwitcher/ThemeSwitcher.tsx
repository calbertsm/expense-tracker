import React, { Component } from "react";
import { Switch, Tooltip, Icon } from "antd";
import { ThemeConsumer } from "ThemeContext";

import config from "config";

const AlternateIcon = Icon.createFromIconfontCN({
	scriptUrl: config.iconFontUrl
});

export default class ThemeSwitcher extends Component {
	render() {
		return (
			<ThemeConsumer>
				{({ theme, toggleTheme }) => (
					<Tooltip placement='bottom' title='Toggle Dark Theme'>
						<Switch key='themeSwitch' checkedChildren={<AlternateIcon type='icon-moon' />} unCheckedChildren={<AlternateIcon type='icon-sun' />} onChange={toggleTheme} />
					</Tooltip>
				)}
			</ThemeConsumer>
		);
	}
}
