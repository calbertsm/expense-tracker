import React, { Component } from "react";

import "./NotFound.scss";

import { Result, Button } from 'antd';

export default class NotFound extends Component {
	render() {
		return (
			<div className="ant-custom-not-found">
				<Result
					status="404"
					title="404"
					subTitle="Sorry, the page you visited does not exist."
					extra={<Button type="primary" href="/">Back Home</Button>}
				/>
			</div>
		);
	}
}
