import React, { Component } from "react";

import "./History.scss";

import { Card, Row, Col, Tabs, DatePicker, Table, Descriptions } from "antd";
import IconTag from "components/IconTag/IconTag";

import { RangePickerValue } from "antd/lib/date-picker/interface";
import moment, { Moment } from "moment";
import { numberFormatter, showApiErrorMessage } from "utils/utils";
import { Expense, Category } from "utils/utils";
import { forEach } from "lodash";
import { get } from "api";

const { TabPane } = Tabs;
const { RangePicker } = DatePicker;

interface Props { }

interface State {
	dateRange: RangePickerValue;
	dateRangeDisabled: boolean;
	historyData: Expense[];
	filter: undefined | {};
	sorter: undefined | {};
	tableIsLoading: boolean;
}

const columns = [
	{
		title: "Date",
		dataIndex: "date",
		width: 150,
		sorter: (a: Expense, b: Expense) => moment(a.date).unix() - moment(b.date).unix(),
		render: (date: string) => moment(date).format('ddd, MMM DD, YYYY @ hh:mm A')
	},
	{
		title: "Description",
		dataIndex: "description",
	},
	{
		title: "Amount",
		dataIndex: "amount",
		width: 120,
		sorter: (a: Expense, b: Expense) => a.amount - b.amount,
		render: (amount: number) => `$ ${numberFormatter(amount)}`,
	}
];

export default class StastisticCard extends Component<Props, State> {
	constructor(props: Props) {
		super(props);
		this.state = {
			dateRange: [moment().startOf("day"), moment().endOf("day")],
			dateRangeDisabled: true,
			filter: undefined,
			sorter: {},
			tableIsLoading: true,
			historyData: []
		};
	}
	componentDidMount = () => {
		this.updateHistoryData(this.state.dateRange[0], this.state.dateRange[1], this.state.dateRangeDisabled);
	}
	categoriesTagRenderer = (categories: Category[] = []) => {
		let tags:JSX.Element[] = [];
		if (categories.length > 0) {
			forEach(categories, (category: Category) => {
				tags.push(<IconTag key={category.id} color={category.color} icon={category.icon}>{category.name}</IconTag>)
			});
		}
		return tags.length > 0 ? tags : "-";
	}
	expandedDataRenderer = (record: Expense) => {
		return <Descriptions size='small'>
			<Descriptions.Item label="Categories" span={12}>
				{this.categoriesTagRenderer(record.categories)}
			</Descriptions.Item>
			<Descriptions.Item label="Notes" span={12}>
				{record.notes}
			</Descriptions.Item>
		</Descriptions>;
	}
	onDateFilterTabChange = (activeKey: string) => {
		let dateRange: RangePickerValue = this.state.dateRange;
		let disableCustomRangePicker: boolean = true;
		switch (activeKey) {
			case "yesterday":
				dateRange = [
					moment()
						.startOf("day")
						.subtract(1, "days"),
					moment()
						.endOf("day")
						.subtract(1, "days")
				];
				break;
			case "thisWeek":
				dateRange = [moment().startOf("week"), moment().endOf("week")];
				break;
			case "thisMonth":
				dateRange = [moment().startOf("month"), moment().endOf("month")];
				break;
			case "custom":
				disableCustomRangePicker = false;
				break;
			case "today":
			default:
				dateRange = [moment().startOf("day"), moment().endOf("day")];
				break;
		}
		this.setState({
			dateRangeDisabled: disableCustomRangePicker,
			dateRange: dateRange
		});
		this.updateHistoryData(dateRange[0], dateRange[1], disableCustomRangePicker);
	};
	onCustomDateRangeChange = (dates: RangePickerValue) => {
		this.setState({
			dateRange: dates
		});
		this.updateHistoryData(dates[0], dates[1]);
	};
	updateHistoryData = (startDate: Moment = moment(), endDate: Moment = moment(), isCustom: boolean = true) => {
		this.setState({
			dateRangeDisabled: isCustom,
			tableIsLoading: true
		});
		get('expenses/history', {
			startDate: startDate.format('YYYY-MM-DD'),
			endDate: endDate.format('YYYY-MM-DD'),
			timezone: moment().format('ZZ')
		}).then((historyResponse) => {
			this.setState({
				tableIsLoading: false,
				historyData: historyResponse.data
			});
		}).catch((error) => {
			this.setState({
				tableIsLoading: false,
				historyData: []
			});
			showApiErrorMessage(error);
		});
	}
	render() {
		return (
			<div className='antd-custom-history-dashboard'>
				<Row gutter={24} style={{ margin: 0 }}>
					<Card>
						<Col span={24}>
							<Tabs
								tabBarGutter={12}
								defaultActiveKey='today'
								onChange={this.onDateFilterTabChange}
								tabBarExtraContent={
									<RangePicker disabled={this.state.dateRangeDisabled} value={this.state.dateRange} onChange={this.onCustomDateRangeChange} />
								}>
								<TabPane tab='Today' key='today' />
								<TabPane tab='Yesterday' key='yesterday' />
								<TabPane tab='This Week' key='thisWeek' />
								<TabPane tab='This Month' key='thisMonth' />
								<TabPane tab='Custom' key='custom' />
							</Tabs>
						</Col>
						<Col span={24}>
							<Table
								bordered={true}
								columns={columns}
								dataSource={this.state.historyData}
								pagination={{
									pageSize: 10,
									size: "",
									total: this.state.historyData.length,
									showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`
								}}
								scroll={{ y: "calc(100% - 55px)" }}
								size='middle'
								expandRowByClick={true}
								expandedRowRender={this.expandedDataRenderer}
								loading={this.state.tableIsLoading}/>
						</Col>
					</Card>
				</Row>
			</div>
		);
	}
}
