import React, { Component, ReactNode } from "react";

import "./StastisticCard.scss";

import { Skeleton, Card, Statistic, Divider } from "antd";

import { numberFormatter } from "utils/utils";

type Props = {
	loading: boolean;
	value: number | string | undefined;
	precision?: number | undefined;
	prefix?: string | ReactNode;
	suffix?: string | ReactNode;
	title?: string | ReactNode;
	moreDataTitle?: string;
	moreDataValue?: string | number;
	moreDataValuePrefix?: string | ReactNode;
	moreDataValueSuffix?: string | ReactNode;
};

export default class StastisticCard extends Component<Props> {
	render() {
		return (
			<div className='antd-custom-statisticCard'>
				<Skeleton active loading={this.props.loading}>
					<Card>
						<Statistic
							className={
								this.props.moreDataValue ? "" : "antd-custom-statisticCard-noMoreData"
							}
							title={this.props.title}
							value={this.props.value}
							precision={this.props.precision}
							prefix={this.props.prefix} suffix={this.props.suffix} />
						{this.props.moreDataValue ? <Divider /> : ""}
						{this.props.moreDataTitle} {this.props.moreDataValuePrefix} {numberFormatter(String(this.props.moreDataValue), this.props.precision)} {this.props.moreDataValueSuffix}
					</Card>
				</Skeleton>
			</div>
		);
	}
}
