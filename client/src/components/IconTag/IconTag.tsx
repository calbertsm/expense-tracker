import React, { Component } from "react";
import { Icon } from "antd";

import config from "config";

import { isEmpty } from "lodash";
import { normalizeHexColor } from "utils/utils";
import color from "color";

const AlternateIcon = Icon.createFromIconfontCN({
	scriptUrl: config.iconFontUrl
});

interface Props {
	color?: string;
	icon?: string;
}

export default class ThemeSwitcher extends Component<Props> {
	iconRenderer = () => {
		return isEmpty(this.props.icon) ? "" : <AlternateIcon type={this.props.icon} theme="twoTone" twoToneColor={normalizeHexColor(this.props.color)} />
	}
	spanRenderer = () => {
		if (isEmpty(this.props.color)) {
			return (
				<span className="ant-custom-icon-tag ant-tag">
					{this.props.children}
				</span>
			)
		} else {
			let colorString = normalizeHexColor(this.props.color);
			return (
				<span className="ant-custom-icon-tag ant-tag" style={{
					color: colorString,
					background: 'transparent',
					borderColor: color(colorString).lighten(0.3).hex()
				}}>
					{this.props.children}
				</span>
			);
		}
	}
	render() {
		return this.spanRenderer();
	}
}
