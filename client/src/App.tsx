import React from 'react';
import Dashboard from 'components/Dashboard/Dashboard';
import './App.scss';

const App: React.FC = () => {
  return (
    <div className="App">
      <Dashboard/>
    </div>
  );
}

export default App;
