const {
	BadRequest
} = require('@feathersjs/errors');
const moment = require('moment');
const {
	isEmpty,
	isNil,
	isObject,
	isArray,
	isNaN,
	isNumber,
	isString,
	isDate,
	isBoolean
} = require('lodash');

exports.validateDateRange = (startDate, endDate) => {
	if (!isEmpty(startDate)) {
		startDate = moment(startDate);
		if (!startDate.isValid()) {
			return new BadRequest('Invalid value for request parameter: \'startDate\'');
		}
	} else {
		return new BadRequest('Missing parameter \'startDate\'');
	}
	if (!isEmpty(endDate)) {
		endDate = moment(endDate);
		if (!endDate.isValid()) {
			return new BadRequest('Invalid value for request parameter: \'endDate\'');
		}
	} else {
		return new BadRequest('Missing parameter \'endDate\'');
	}
	if (endDate.isBefore(startDate)) {
		return new BadRequest('Invalid values for request parameters: \'endDate\' is before \'startDate\'');
	}
	return -1;
};

exports.getDatesFromDateRange = (startDate, endDate) => {
	let result = [];
	if (startDate.isAfter(endDate)) {
		throw new Error('Start Date is after End Date');
	}
	let dayDiff = endDate.diff(startDate, 'days');
	for (let index = 0; index <= dayDiff; index++) {
		result.push(moment(startDate).add(index, 'days').format('YYYY-MM-DD'));
	}
	return result;
};

exports.validateIfParamIsDefined = (paramName, value, paramConstraints) => {
	if (isNil(value)) {
		return new BadRequest(`Missing parameter '${paramName}'`);
	} else if (!isEmpty(paramConstraints)) {
		let isValid = false;
		switch (paramConstraints.type.toLowerCase()) {
		case 'number':
			isValid = !isNaN(value) && isNumber(value);
			break;
		case 'string':
			isValid = isString(value);
			break;
		case 'boolean':
			isValid = isBoolean(value);
			break;
		case 'array':
			isValid = isArray(value);
			break;
		case 'date':
			isValid = isDate(value);
			break;
		default:
			isValid = isObject(value);
			break;
		}
		if (isValid) {
			return new BadRequest(`Invalid value for request parameter: '${paramName}'`);
		}
	}
	return -1;
};
