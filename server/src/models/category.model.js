/* eslint-disable no-console */

// category-model.js - A KnexJS
// 
// See http://knexjs.org/
// for more of what you can do here.
module.exports = function (app) {
	const db = app.get('knexClient');
	const tableName = 'category';
	db.schema.hasTable(tableName).then(exists => {
		if(!exists) {
			db.schema.createTable(tableName, table => {
				table.uuid('id').notNullable().primary('Category_pkey').unique().defaultTo('uuid_generate_v1()');
				table.string('name', 32).notNullable().unique();
				table.string('description', 50);
				table.string('color', 6);
				table.string('icon', 32);
			})
				.then(() => console.log(`Created ${tableName} table`))
				.catch(e => console.error(`Error creating ${tableName} table`, e));
		}
	});
	return db;
};
