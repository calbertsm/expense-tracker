/* eslint-disable no-console */

// expense-model.js - A KnexJS
// 
// See http://knexjs.org/
// for more of what you can do here.
module.exports = function (app) {
	const db = app.get('knexClient');
	const tableName = 'expense';
	db.schema.hasTable(tableName).then(exists => {
		if (!exists) {
			db.schema.createTable(tableName, table => {
				table.uuid('id').notNullable().primary('Expense_pkey').unique().defaultTo('uuid_generate_v1()');
				table.string('description', 30).notNullable();
				table.decimal('amount', 11, 2).notNullable();
				table.text('notes');
				table.datetime('date', {
					precision: 6,
					useTz: true
				}).notNullable();
			})
				.then(() => console.log(`Created ${tableName} table`))
				.catch(e => console.error(`Error creating ${tableName} table`, e));
		}
	});
	return db;
};
