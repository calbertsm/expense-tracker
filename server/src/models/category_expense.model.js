/* eslint-disable no-console */

// category_expense-model.js - A KnexJS
// 
// See http://knexjs.org/
// for more of what you can do here.
module.exports = function (app) {
	const db = app.get('knexClient');
	const tableName = 'category_expense';
	db.schema.hasTable(tableName).then(exists => {
		if (!exists) {
			db.schema.createTable(tableName, table => {
				table.uuid('id').notNullable().primary('Category_Expense_pkey').unique().defaultTo('uuid_generate_v1()');
				table.uuid('category_id').notNullable();
				table.uuid('expense_id').notNullable();
				table.foreign('category_id').references('category.id');
				table.foreign('expense_id').references('expense.id');
			})
				.then(() => console.log(`Created ${tableName} table`))
				.catch(e => console.error(`Error creating ${tableName} table`, e));
		}
	});
	return db;
};
