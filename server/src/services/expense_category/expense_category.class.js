const { Service } = require('feathers-knex');
exports.ExpenseCategory = class ExpenseCategory extends Service {
	constructor(options) {
		super({
			...options,
			name: 'expenseCategory'
		});
	}
};
