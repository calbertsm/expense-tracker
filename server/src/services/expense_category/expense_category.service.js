// Initializes the `expense` service on path `/expense`
const moment = require('moment');
const { isEmpty, groupBy, map, orderBy, capitalize } = require('lodash');
const { validateDateRange, validateIfParamIsDefined } = require('../../utils');
const { GeneralError } = require('@feathersjs/errors');

module.exports = function (app) {
	const db = app.get('knexClient');
	app.use('/expenses/byCategoryName', {
		async find(params) {
			let isValidParam = validateIfParamIsDefined('name', params.query.name, {type: 'array'});
			if(isValidParam !== -1) {
				throw isValidParam;
			}
			let oSubQuery = db('category_expense')
				.withSchema('public')
				.innerJoin('category', 'category.id', 'category_expense.category_id')
				.select('category_expense.expense_id');
			if (Array.isArray(params.query.name)) {
				oSubQuery = oSubQuery
					.where('category.name', 'like', `%${params.query.name[0]}%`)
					.andWhere(function () {
						for (let i = 1; i <= params.query.name.length; i++) {
							this.where('category.name', 'like', `%${params.query.name[i]}%`);
						}
					});
			} else {
				let aCategoryName = params.query.name.trim().split(',');
				oSubQuery = oSubQuery
					.where('category.name', 'like', `%${aCategoryName[0]}%`)
					.orWhere(function () {
						for (let i = 1; i < aCategoryName.length; i++) {
							this.where('category.name', 'like', `%${aCategoryName[i]}%`);
						}
					});
			}
			return db
				.withSchema('public')
				.select()
				.from('expense')
				.whereIn(
					'id',
					oSubQuery
				)
				.orderBy('date', 'desc');
		}
	});
	app.use('/expenses/byCategoryId', {
		async find(params) {
			let isValidParam = validateIfParamIsDefined('id', params.query.id, {type: 'array'});
			if(isValidParam !== -1) {
				throw isValidParam;
			}
			let oSubQuery = db('category_expense')
				.withSchema('public')
				.innerJoin('category', 'category.id', 'category_expense.category_id')
				.select('category_expense.expense_id');
			if (Array.isArray(params.query.id)) {
				oSubQuery = oSubQuery
					.where('category.id', params.query.id[0])
					.andWhere(function () {
						for (let i = 1; i <= params.query.id.length; i++) {
							this.where('category.id', params.query.id[i]);
						}
					});
			} else {
				let aCategoryId = params.query.id.trim().split(',');
				oSubQuery = oSubQuery
					.where('category.id', aCategoryId[0])
					.orWhere(function () {
						for (let i = 1; i < aCategoryId.length; i++) {
							this.where('category.id', aCategoryId[i]);
						}
					});
			}
			return db
				.withSchema('public')
				.select()
				.from('expense')
				.whereIn(
					'id',
					oSubQuery
				)
				.orderBy('date', 'desc');
		}
	});
	app.use('/expenses/history', {
		async find(params) {
			if (isEmpty(params.query.timezone)) {
				params.query.timezone = '+0000';
			}
			let validateParams = validateDateRange(params.query.startDate, params.query.endDate);
			if (validateParams !== -1) {
				throw validateParams;
			}
			let startDate = moment.utc(`${params.query.startDate} 00:00:00${params.query.timezone}`),
				endDate = moment.utc(`${params.query.endDate} 23:59:59${params.query.timezone}`);
			return new Promise((resolve, reject) => {
				db('expense')
					.withSchema('public')
					.innerJoin('category_expense', 'expense.id', 'category_expense.expense_id')
					.innerJoin('category', 'category.id', 'category_expense.category_id')
					.select('expense.id', 'expense.description', 'expense.date', 'expense.notes', 'expense.amount', 'category.id as category_id', 'category.name', 'category.icon', 'category.color')
					.whereBetween('expense.date', [
						startDate.format('YYYY-MM-DD HH:mm:ssZZ'),
						endDate.format('YYYY-MM-DD HH:mm:ssZZ')
					])
					.orderBy('expense.date', 'desc')
					.then((data) => {
						let result = groupBy(data, 'id');
						result = map(result, (expenses) => {
							return {
								id: expenses[0].id,
								amount: parseFloat(expenses[0].amount),
								date: expenses[0].date,
								description: expenses[0].description.trim(),
								notes: isEmpty(expenses[0].notes) ? '' : expenses[0].notes.trim(),
								categories: orderBy(map(expenses, (expense) => {
									return {
										id: expense.category_id,
										name: capitalize(expense.name.trim()),
										color: expense.color,
										icon: expense.icon
									};
								}), 'name')
							};
						});
						resolve(orderBy(result, 'date', 'desc'));
					})
					.catch((error) => {
						reject(new GeneralError(`${error.routine ? error.routine : error.message}`));
					});
			});
		}
	});
};
