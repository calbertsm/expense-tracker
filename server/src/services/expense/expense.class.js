const { Service } = require('feathers-knex');

exports.Expense = class Expense extends Service {
	constructor(options) {
		super({
			...options,
			name: 'expense'
		});
	}
};
