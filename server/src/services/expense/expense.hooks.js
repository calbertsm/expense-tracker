const {
	map
} = require('lodash');
const {
	GeneralError
} = require('@feathersjs/errors');
var categories;
module.exports = {
	before: {
		all: [],
		find: [],
		get: [],
		create: [(context) => {
			categories = context.data.categories;
			delete context.data.categories;
			return context;
		}],
		update: [],
		patch: [],
		remove: []
	},

	after: {
		all: [],
		find: [],
		get: [],
		create: [(context) => {
			let expenseId = context.result.id;
			let promises = map(categories, (categoryId) => {
				return context.app.service('category-expense').create({
					expense_id: expenseId,
					category_id: categoryId
				});
			});
			Promise.all(promises).then(() => {
				context.result.categories = categories;
				return context;
			}).catch((error) => {
				throw new GeneralError(`Error: ${error.routine}`);
			});
		}],
		update: [],
		patch: [],
		remove: []
	},

	error: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: []
	}
};
