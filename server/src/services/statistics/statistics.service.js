// Initializes the `expense` service on path `/expense`
const moment = require('moment');
const {
	isEmpty,
	map,
	filter,
	forEach,
	assign,
	fill,
	findIndex,
	orderBy
} = require('lodash');
const {
	validateDateRange,
	getDatesFromDateRange
} = require('../../utils');
const {
	GeneralError
} = require('@feathersjs/errors');

module.exports = function (app) {
	const db = app.get('knexClient');

	// Initialize our service with any options it requires
	app.use('/statistics', {
		async find(params) {
			if (isEmpty(params.query) || isEmpty(params.query.timezone)) {
				params.query = {
					timezone: '+0000'
				};
			}
			let oToday = moment.utc(`${moment.utc().format('YYYY-MM-DD')} 00:00:00${params.query.timezone}`);
			let oMonth = moment.utc(
				`${moment
					.utc()
					.startOf('month')
					.format('YYYY-MM-DD')} 00:00:00${params.query.timezone}`
			);
			return Promise.all([
				db('expense')
					.withSchema('public')
					.sum('amount'),
				db('category_expense')
					.withSchema('public')
					.innerJoin('category', 'category.id', 'category_expense.category_id')
					.innerJoin('expense', 'expense.id', 'category_expense.expense_id')
					.select('expense.id', 'expense.description', 'expense.amount', 'expense.notes', 'expense.date', 'category.name as category_name', 'category.icon as category_icon', 'category.color as category_color')
					.where(
						'category_expense.expense_id',
						'=',
						db('expense')
							.withSchema('public')
							.select('id')
							.orderBy([{
								column: 'amount',
								order: 'desc'
							},
							{
								column: 'date',
								order: 'desc'
							}
							])
							.limit(1)
					)
					.orderBy([{
						column: 'expense.amount',
						order: 'desc'
					},
					{
						column: 'expense.date',
						order: 'desc'
					}
					])
					.limit(1),
				db('category_expense')
					.withSchema('public')
					.innerJoin('category', 'category.id', 'category_expense.category_id')
					.innerJoin('expense', 'expense.id', 'category_expense.expense_id')
					.select('expense.id', 'expense.description', 'expense.amount', 'expense.notes', 'expense.date', 'category.name as category_name', 'category.icon as category_icon', 'category.color as category_color')
					.where(
						'category_expense.expense_id',
						'=',
						db('expense')
							.withSchema('public')
							.select('id')
							.whereBetween('date', [
								oToday.format('YYYY-MM-DD HH:mm:ssZZ'),
								moment(oToday)
									.add(1, 'days')
									.format('YYYY-MM-DD HH:mm:ssZZ')
							])
							.orderBy([{
								column: 'amount',
								order: 'desc'
							},
							{
								column: 'date',
								order: 'desc'
							}
							])
							.limit(1)
					)
					.orderBy([{
						column: 'expense.amount',
						order: 'desc'
					},
					{
						column: 'expense.date',
						order: 'desc'
					}
					])
					.limit(1),
				db('expense')
					.withSchema('public')
					.avg('amount')
					.whereBetween('date', [
						oMonth.format('YYYY-MM-DD HH:mm:ssZZ'),
						moment(oMonth)
							.add(1, 'month')
							.format('YYYY-MM-DD HH:mm:ssZZ')
					]),
				db('expense')
					.withSchema('public')
					.avg('amount')
					.whereBetween('date', [
						moment(oMonth)
							.subtract(1, 'month')
							.format('YYYY-MM-DD HH:mm:ssZZ'),
						oMonth.format('YYYY-MM-DD HH:mm:ssZZ')
					]),
				db('expense')
					.withSchema('public')
					.avg('amount')
					.whereBetween('date', [
						oToday.format('YYYY-MM-DD HH:mm:ssZZ'),
						moment(oToday)
							.add(1, 'days')
							.format('YYYY-MM-DD HH:mm:ssZZ')
					]),
				db('expense')
					.withSchema('public')
					.avg('amount')
					.whereBetween('date', [
						moment(oToday)
							.subtract(1, 'days')
							.format('YYYY-MM-DD HH:mm:ssZZ'),
						oToday.format('YYYY-MM-DD HH:mm:ssZZ')
					]),
				db('expense')
					.withSchema('public')
					.innerJoin('category_expense', 'expense.id', 'category_expense.expense_id')
					.innerJoin('category', 'category.id', 'category_expense.category_id')
					.select(db.raw('category.id, category.name, category.icon, category.color, sum(expense.amount) AS total'))
					.groupBy('category.id')
					.orderBy('total', 'desc')
					.limit(1)
			])
				.then(([totalExpensesAllTime, biggestExpenseAllTime, biggestExpenseToday, averageExpenseThisMonth, averageExpenseLastMonth, averageExpenseToday, averageExpenseYesterday, mostExpendedCategory]) => {
					let emptyExpenseObject = {
						id: '-',
						description: '-',
						amount: 0,
						notes: '-',
						date: '-',
						categories: []
					};
					let emptyCategoryObject = {
						id: '-',
						name: '-',
						icon: '-',
						color: '-',
						total: 0
					};
					let result = {
						totalExpensesAllTime: isEmpty(totalExpensesAllTime[0].sum) ? 0 : parseFloat(totalExpensesAllTime[0].sum),
						biggestExpenseAllTime: isEmpty(biggestExpenseAllTime) ? emptyExpenseObject : {
							id: biggestExpenseAllTime[0].id,
							description: biggestExpenseAllTime[0].description.trim(),
							amount: parseFloat(biggestExpenseAllTime[0].amount),
							notes: isEmpty(biggestExpenseAllTime[0].notes) ? '' : biggestExpenseAllTime[0].notes.trim(),
							date: biggestExpenseAllTime[0].date,
							categories: map(biggestExpenseAllTime, (expense) => {
								return {
									name: expense.category_name,
									icon: expense.category_icon,
									color: expense.category_color
								};
							})
						},
						biggestExpenseToday: isEmpty(biggestExpenseToday) ? emptyExpenseObject : {
							id: biggestExpenseToday[0].id,
							description: biggestExpenseToday[0].description.trim(),
							amount: parseFloat(biggestExpenseToday[0].amount),
							notes: isEmpty(biggestExpenseToday[0].notes) ? '' : biggestExpenseToday[0].notes.trim(),
							date: biggestExpenseToday[0].date,
							categories: map(biggestExpenseToday, (expense) => {
								return {
									name: expense.category_name,
									icon: expense.category_icon,
									color: expense.category_color
								};
							})
						},
						averageExpenseThisMonth: isEmpty(averageExpenseThisMonth[0].avg) ? 0 : parseFloat(averageExpenseThisMonth[0].avg),
						averageExpenseLastMonth: isEmpty(averageExpenseLastMonth[0].avg) ? 0 : parseFloat(averageExpenseLastMonth[0].avg),
						averageExpenseToday: isEmpty(averageExpenseToday[0].avg) ? 0 : parseFloat(averageExpenseToday[0].avg),
						averageExpenseYesterday: isEmpty(averageExpenseYesterday[0].avg) ? 0 : parseFloat(averageExpenseYesterday[0].avg),
						mostExpendedCategory: isEmpty(mostExpendedCategory[0]) ? emptyCategoryObject : mostExpendedCategory[0]
					};
					return result;
				})
				.catch((error) => {
					throw new GeneralError(`${error.code}: ${error.routine}`);
				});
		}
	});
	app.use('/statistics/expenses/category', {
		async find() {
			return db('category_expense')
				.withSchema('public')
				.innerJoin('expense', 'category_expense.expense_id', 'expense.id')
				.rightJoin('category', 'category.id', 'category_expense.category_id')
				.select(db.raw('category.id, category.name, category.description, category.icon, category.color, coalesce(count(category_expense.expense_id),0) as total_expenses, coalesce(sum(expense.amount),0) as total_amount'))
				.groupBy('category.id')
				.then((totalsByCategory) => {
					return orderBy(map(totalsByCategory, (category) => {
						return {
							color: category.color.trim(),
							description: category.description.trim(),
							icon: category.icon.trim(),
							id: category.id.trim(),
							name: category.name.trim(),
							total_amount: parseFloat(category.total_amount),
							total_expenses: parseFloat(category.total_expenses)
						};
					}), 'total_expenses', 'desc');
				}).catch((error) => {
					throw new GeneralError(`Error ${error.code}: ${error.routine}`);
				});
		}
	});
	app.use('/statistics/expenses/totals', {
		async find(params) {
			if (isEmpty(params.query.timezone)) {
				params.query.timezone = '+0000';
			}
			let validateParams = validateDateRange(params.query.startDate, params.query.endDate);
			if (validateParams !== -1) {
				throw validateParams;
			}
			let startDate = moment.utc(`${params.query.startDate} 00:00:00${params.query.timezone}`),
				endDate = moment.utc(`${params.query.endDate} 23:59:59${params.query.timezone}`);
			return Promise.all([
				db('expense')
					.withSchema('public')
					.select('*')
					.whereBetween('date', [
						startDate.format('YYYY-MM-DD HH:mm:ssZZ'),
						endDate.format('YYYY-MM-DD HH:mm:ssZZ')
					]),
				db('category')
					.withSchema('public')
					.select('*'),
				db('expense')
					.withSchema('public')
					.innerJoin('category_expense', 'expense.id', 'category_expense.expense_id')
					.select('category_expense.expense_id', 'expense.date', 'category_expense.category_id')
					.whereBetween('expense.date', [
						startDate.format('YYYY-MM-DD HH:mm:ssZZ'),
						endDate.format('YYYY-MM-DD HH:mm:ssZZ')
					])
			])
				.then(([totalsByDate, categories, expensesBetweenDates]) => {
					let dates = getDatesFromDateRange(startDate, endDate);
					let result = {
						totalsByDate: [],
						totalsPerCategoryByDate: map(categories, (category) => {
							return assign({
								totalsPerDate: fill(new Array(dates.length), 0)
							}, category);
						})
					};
					let beginDate, finishDate;
					forEach(dates, (date, index) => {
						beginDate = moment.utc(`${date} 00:00:00${params.query.timezone}`);
						finishDate = moment.utc(`${date} 23:59:59${params.query.timezone}`);
						result.totalsByDate.push({
							date: date,
							total: filter(totalsByDate, (expense) => {
								let expenseDate = moment.utc(expense.date);
								return expenseDate.isSameOrAfter(beginDate) && expenseDate.isBefore(finishDate);
							}).length,
						});
						forEach(filter(expensesBetweenDates, (expenseCategory) => {
							let expenseDate = moment.utc(expenseCategory.date);
							return expenseDate.isSameOrAfter(beginDate) && expenseDate.isBefore(finishDate);
						}), (expenseCategory) => {
							let categoryIndex = findIndex(result.totalsPerCategoryByDate, {
								id: expenseCategory.category_id
							});
							if (categoryIndex !== -1) {
								result.totalsPerCategoryByDate[categoryIndex].totalsPerDate[index]++;
							}
						});
					});
					return result;
				})
				.catch((error) => {
					throw new GeneralError(`Error ${error.code}: ${error.routine}`);
				});
		}
	});
	app.use('/statistics/category/top', {
		async find() {
			return db('expense')
				.withSchema('public')
				.innerJoin('category_expense', 'expense.id', 'category_expense.expense_id')
				.innerJoin('category', 'category.id', 'category_expense.category_id')
				.select(db.raw('category.id, category.name, category.icon, category.color, sum(expense.amount) AS total'))
				.whereBetween('expense.date', [moment.utc().startOf('month'), moment.utc().endOf('month')])
				.groupBy('category.id')
				.orderBy('total', 'desc')
				.limit(1)
				.then((mostExpendedCategory) => {
					return {
						id: mostExpendedCategory[0].id,
						name: mostExpendedCategory[0].name,
						icon: mostExpendedCategory[0].icon,
						color: mostExpendedCategory[0].color,
						total: parseFloat(mostExpendedCategory[0].total)
					};
				}).catch((error) => {
					throw new GeneralError(`${error.code}: ${error.routine}`);
				});
		}
	});
};
