const expense = require('./expense/expense.service.js');
const category = require('./category/category.service.js');
const categoryExpense = require('./category_expense/category_expense.service.js');
const expenseCategory = require('./expense_category/expense_category.service.js');
const statistics = require('./statistics/statistics.service.js');

module.exports = function (app) {
	app.configure(expense);
	app.configure(category);
	app.configure(categoryExpense);
	app.configure(expenseCategory);
	app.configure(statistics);
};
