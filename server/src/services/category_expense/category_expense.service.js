// Initializes the `CategoryExpense` service on path `/category-expense`
const { CategoryExpense } = require('./category_expense.class');
const createModel = require('../../models/category_expense.model');
const hooks = require('./category_expense.hooks');

module.exports = function (app) {
	const Model = createModel(app);

	const options = {
		Model
	};

	// Initialize our service with any options it requires
	app.use('/category-expense', new CategoryExpense(options, app));

	// Get our initialized service so that we can register hooks
	const service = app.service('category-expense');

	service.hooks(hooks);
};
