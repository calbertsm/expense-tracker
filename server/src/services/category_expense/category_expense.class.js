const { Service } = require('feathers-knex');

exports.CategoryExpense = class CategoryExpense extends Service {
	constructor(options) {
		super({
			...options,
			name: 'category_expense'
		});
	}
};
