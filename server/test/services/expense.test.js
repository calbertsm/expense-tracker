const app = require('../../src/app');
const assert = require('assert');
const moment = require('moment');

describe('\'expense\' service', () => {
	let expense;
	let dummyExpense = {
		description: 'Test Expense',
		amount: 12.50,
		notes: 'Test Notes',
		date: moment().toDate(),
		categories: []
	};
	it('registered the service', () => {
		const service = app.service('expense');
		expect(service).toBeTruthy();
		assert.ok(service, 'Expense service registered successfully');
	});

	test('creates an expense with no categories', async () => {
		expense = await app.service('expense').create(dummyExpense);
		expect(expense.id).toBeTruthy();
		expect(dummyExpense).toHaveProperty('description', dummyExpense.description);
	});

	test('updates an expense', async () => {
		// eslint-disable-next-line require-atomic-updates
		expense = await app.service('expense').update(expense.id, {
			description: 'Test Expense 2',
			amount: 12.50,
			notes: 'Test Notes',
			date: moment().toDate(),
			categories: []
		});
		expect(expense.description.trim()).toBe('Test Expense 2');
	});

	test('deletes an expense', async () => {
		await app.service('expense').remove(expense.id);
		// eslint-disable-next-line require-atomic-updates
		expense = app.service('expense').get(expense.id);
		expect(expense).toBeTruthy();
	});
});
