const app = require('../../src/app');
const assert = require('assert');

describe('\'category\' service', () => {
	let category;
	let dummyCategory = {
		name: `Test Category ${Math.floor((Math.random() * 10000) + 1)}`,
		description: 'Test Category Description'
	};

	it('registered the service', () => {
		const service = app.service('category');
		expect(service).toBeTruthy();
		assert.ok(service, 'Category service registered successfully');
	});

	test.skip('creates a category', async () => {
		category = await app.service('category').create(dummyCategory);
		expect(category.id).toBeTruthy();
		expect(category).toHaveProperty('name', dummyCategory.name);
	});

	test.skip('updates a category', async () => {
		// eslint-disable-next-line require-atomic-updates
		category = await app.service('category').update(category.id, {
			name: dummyCategory.name,
			description: 'Test',
			color: 'ffffff'
		});
		expect(category.description.trim()).toBe('Test');
		expect(category.color.trim()).toBe('ffffff');
	});

	test.skip('deletes a category', async () => {
		await app.service('category').remove(category.id);
		// eslint-disable-next-line require-atomic-updates
		category = app.service('category').get(category.id);
		expect(category).toBeTruthy();
	});
});
