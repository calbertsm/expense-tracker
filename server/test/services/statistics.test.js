const app = require('../../src/app');
const assert = require('assert');

describe('\'statistics\' service', () => {
	it('registered the service', () => {
		const service = app.service('statistics');
		expect(service).toBeTruthy();
		assert.ok(service, 'Statistics service registered successfully');
	});

	test('gets statistics', async () => {
		let expenses = await app.service('statistics').find();
		expect(expenses).toHaveProperty('biggestExpenseAllTime');
	});
});

describe('\'statistics/expenses/category\' service', () => {
	it('registered the service', () => {
		const service = app.service('statistics/expenses/category');
		expect(service).toBeTruthy();
		assert.ok(service, 'Total Expensee per Category service registered successfully');
	});

	test('get total expenses (amount and count) per category', async () => {
		await app.service('statistics/expenses/category').find()
			.then((categories) => {
				expect(categories.data.length).toBeGreaterThanOrEqual(0);
			})
			.catch((error) => {
				expect(error).toHaveProperty('message');
			});
		
	});
});

describe('\'statistics/expenses/totals\' service', () => {
	it('registered the service', () => {
		const service = app.service('statistics/expenses/totals');
		expect(service).toBeTruthy();
		assert.ok(service, 'Expenses Totals Statistics service registered successfully');
	});

	test('gets expenses totals statistics by date (sending proper params)', async () => {
		let expenses = await app.service('statistics/expenses/totals').find({
			query: {
				startDate: '2019-09-09',
				endDate: '2019-10-20'
			}
		});
		expect(expenses).toHaveProperty('totalsByDate');
	});

	test('gets expenses totals statistics by date  (sending incorrect startDate)', async () => {
		await expect(app.service('statistics/expenses/totals').find({
			query: {
				startDate: '2011-34-09',
				endDate: '2015-10-20'
			}
		})).rejects.toThrow();
	});

	test('gets expenses totals statistics by date (sending incorrect endDate)', async () => {
		await expect(app.service('statistics/expenses/totals').find({
			query: {
				startDate: '2016-09-09',
				endDate: '2015-10-20'
			}
		})).rejects.toThrow();
	});
});

describe('\'statistics/category/top\' service', () => {
	it('registered the service', () => {
		const service = app.service('statistics/category/top');
		expect(service).toBeTruthy();
		assert.ok(service, 'Most Expended Category service registered successfully');
	});

	test('gets expenses history by date (sending proper params)', async () => {
		let expenses = await app.service('statistics/category/top').find();
		expect(expenses).toHaveProperty('id');
	});
});