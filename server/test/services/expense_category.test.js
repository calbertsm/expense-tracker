const app = require('../../src/app');
const assert = require('assert');

describe('\'expenses/byCategoryName\' service', () => {
	it('registered the service', () => {
		const service = app.service('expenses/byCategoryName');
		expect(service).toBeTruthy();
		assert.ok(service, 'Expense by Category Name service registered successfully');
	});

	test('gets expenses by category name (sending proper params)', async () => {
		let expenses = await app.service('expenses/byCategoryName').find({
			query: {
				name: 'drinks'
			}
		});
		expect(expenses.length).toBeGreaterThanOrEqual(0);
	});

	test('gets expenses by category name (sending incorrect params)', async () => {
		await expect(app.service('expenses/byCategoryName').find({
			query: {
				names: 'drinks'
			}
		})).rejects.toThrow();
	});
});

describe('\'expenses/byCategoryId\' service', () => {
	it('registered the service', () => {
		const service = app.service('expenses/byCategoryId');
		expect(service).toBeTruthy();
		assert.ok(service, 'Expense by Category Ids service registered successfully');
	});

	test('gets expenses by category ids (sending proper params)', async () => {
		let expenses = await app.service('expenses/byCategoryId').find({
			query: {
				id: '8b51b936-1058-45dd-b637-306e3d9a0cdc'
			}
		});
		expect(expenses.length).toBeGreaterThanOrEqual(0);
	});

	test('gets expenses by category ids (sending incorrect params)', async () => {
		await expect(app.service('expenses/byCategoryId').find({
			query: {
				idss: '8b51b936-105'
			}
		})).rejects.toThrow();
	});
});

describe('\'expenses/history\' service', () => {
	it('registered the service', () => {
		const service = app.service('expenses/history');
		expect(service).toBeTruthy();
		assert.ok(service, 'Expenses History by Date service registered successfully');
	});

	test('gets expenses history by date (sending proper params)', async () => {
		let expenses = await app.service('expenses/history').find({
			query: {
				startDate: '2016-09-09',
				endDate: '2019-10-20'
			}
		});
		expect(expenses.length).toBeGreaterThanOrEqual(0);
	});

	test('gets expenses by category name (sending incorrect startDate)', async () => {
		await expect(app.service('expenses/history').find({
			query: {
				startDate: '2011-34-09',
				endDate: '2015-10-20'
			}
		})).rejects.toThrow();
	});

	test('gets expenses by category name (sending incorrect endDate)', async () => {
		await expect(app.service('expenses/history').find({
			query: {
				startDate: '2016-09-09',
				endDate: '2015-10-20'
			}
		})).rejects.toThrow();
	});
});