const app = require('../../src/app');
const assert = require('assert');

describe('\'category-expense\' service', () => {
	it('registered the service', () => {
		const service = app.service('category-expense');
		expect(service).toBeTruthy();
		assert.ok(service, 'Category-Expense service registered successfully');
	});
});
